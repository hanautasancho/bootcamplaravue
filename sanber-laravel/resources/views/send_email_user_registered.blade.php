<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <p>Terimakasih {{ $name }} telah mendaftar di aplikasi kami. untuk melanjutkan verifikasi email, ketikan kode otp berikut:</p>
    <br>
    <p><b>KODE OTP:</b><br>{{ $codeOtp }}</p>
  </body>
</html>