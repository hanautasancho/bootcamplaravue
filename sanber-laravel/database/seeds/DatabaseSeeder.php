<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            BlogSeeder::class
            // CampaignSeeder::class,
            // RoleSeeder::class,
            // UserSeeder::class,
        ]);
    }
}
