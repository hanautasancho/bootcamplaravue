<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin1',
            'email' => 'admin1@gmail.com',
            'password' => Hash::make('admin'),
            'role_id' => '107282b2-b581-4c71-9a16-410392d76e49'
        ]);

        User::create([
            'name' => 'admin2',
            'email' => 'admin2@gmail.com',
            'password' => Hash::make('admin'),
            'role_id' => '107282b2-b581-4c71-9a16-410392d76e49'
        ]);

        User::create([
            'name' => 'dimas',
            'email' => 'dimas@gmail.com',
            'password' => Hash::make('dimas'),
        ]);
    }
}
