<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\OtpCode;
use App\User;
use Illuminate\Http\Request;
use App\Events\UserRegistered;


class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        $user = User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
        ]);
        
        $date = date("Y-m-d H:i:s");

        OtpCode::create([
            'otp' => rand(100000,999999),
            'user_id' => $user->id,
            'valid_until' => date("Y-m-d H:i:s", strtotime($date." +5 minutes"))
        ]);

        event(new UserRegistered($user));

        return response()->json([
            'response_code'=> '00',
            'response_message' => 'silahkan cek email',
            'data' => [
                'user' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'email_verified_at' => $user->email_verified_at,
                    'created_at' => $user->created_at,
                    'updated_at' => $user->updated_at,
                ]
            ]
        ]);
    }
}
