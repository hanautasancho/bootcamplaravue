<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\OtpCode;
use App\User;
use Illuminate\Http\Request;
use App\Events\UserRegenerated;


class RegenerateOtpController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $user = User::where('email', $request->email)->first();
        $date = date("Y-m-d H:i:s");

        if(empty($user->email_verified_at)) {
            $otp_code = OtpCode::where('user_id', $user->id)->first();
            $otp_code->otp = rand(100000,999999);
            $otp_code->valid_until = date("Y-m-d H:i:s", strtotime($date." +5 minutes"));
            $otp_code->save();

            event(new UserRegenerated($user));

            return response()->json([
                'response_code'=> '00',
                'response_message' => 'silahkan cek email',
                'data' => [
                    'user' => [
                        'id' => $user->id,
                        'name' => $user->name,
                        'email' => $user->email,
                        'email_verified_at' => $user->email_verified_at,
                        'created_at' => $user->created_at,
                        'updated_at' => $user->updated_at,
                    ]
                ]
            ]);
        }

        return response()->json([
            'response_code'=> '01',
            'response_message' => 'email sudah terverifikasi',
        ]);
    }
}
