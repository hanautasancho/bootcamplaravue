<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\OtpCode;
use Illuminate\Http\Request;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $user = OtpCode::where('otp', $request->otp)->first();

        if($user) { 

            $date = date("Y-m-d H:i:s");

            if($date > $user->valid_until) {
                return response()->json([
                    'response_code'=> '01',
                    'response_message' => 'kode otp sudah tidak berlaku, silahkan generate ulang',
                ]);
            }

            return response()->json([
                'response_code'=> '00',
                'response_message' => 'berhasil verifikasi',
            ]);
        } else {
            return response()->json([
                'response_code'=> '01',
                'response_message' => 'OTP Code tidak ditemukan',
            ]);
        }

    }
}
