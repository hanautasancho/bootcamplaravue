<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $token = auth()->attempt($request->only('email', 'password'));

        if(!$token) {
            return response()->json([
                'response_code'=> '01',
                'response_message' => 'gagal melakukan login'
            ]);
        }
        
        $user = User::where('email', $request->email)->first();

        if(empty($user->email_verified_at)) {
            return response()->json([
                'response_code'=> '01',
                'response_message' => 'email belum diverifikasi'
            ]);
        }   

        return response()->json([
            'response_code'=> '00',
            'response_message' => 'user berhasil login',
            'data' => [
                'token' => $token,
                'user' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'email_verified_at' => $user->email_verified_at,
                    'created_at' => $user->created_at,
                    'updated_at' => $user->updated_at,
                ]
            ]
        ]);
    }
}
