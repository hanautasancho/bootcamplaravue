<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function random($count) {
        $blogs = Blog::select('*')->inRandomOrder()->limit($count)->get();

        $data['blogs'] = $blogs;

        return response()->json([
            'response_code' => '00',
            'response_message' => 'data blog berhasil ditampilkan',
            'data' => $data
        ], 200);
    }
}
