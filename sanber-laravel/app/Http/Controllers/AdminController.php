<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(Request $request) {
        return "admin dashboard";
    }
}
