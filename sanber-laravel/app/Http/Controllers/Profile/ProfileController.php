<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $user = $request->user();

        return response()->json([
            'response_code'=> '00',
            'response_message' => 'profile berhasil ditampilkan',
            'data' => [
                'profile' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'email_verified_at' => $user->email_verified_at,
                    'created_at' => $user->created_at,
                    'updated_at' => $user->updated_at,
                ]
            ]
        ]);
    }

    public function update(Request $request)
    {
        $validation = Validator::make($request->all() ,[
            'name'   =>  'string',
            'photo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
         ]);

        if($validation->fails()) {
            return response()->json([
                'response_code'=> '01',
                'response_message' => 'gagal update profile',
            ]);
        }

        //update
        $user = $request->user();
        if($request->name) {
            $user->name = $request->name;
        }

        if($request->photo) {
            $imageName = time().'.'.$request->photo->extension();  
            $request->photo->move(public_path('photos/users/photo-profile'), $imageName);
            $user->photo = 'photos/users/photo-profile/'.$imageName;
        }

        $user->save();

        return response()->json([
            'response_code'=> '00',
            'response_message' => 'profile berhasil diupdate',
            'data' => [
                'profile' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'email_verified_at' => $user->email_verified_at,
                    'created_at' => $user->created_at,
                    'updated_at' => $user->updated_at,
                ]
            ]
        ]);
    }
}
