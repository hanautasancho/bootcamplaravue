<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class VerificationEmail
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::select('email_verified_at')->where('id', $request->id)->first();

        if (empty($user->email_verified_at)) {
            return redirect('/verify');
        }

        return $next($request);
    }
}
