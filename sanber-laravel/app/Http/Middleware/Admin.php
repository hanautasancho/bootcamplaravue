<?php

namespace App\Http\Middleware;

use App\User;
use App\Role;
use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::select('role_id')->where('id', $request->id)->first();
        $role = Role::find($user->role_id);

        if ($role->name == "ADMIN") {
            return $next($request);
        }
        
        return redirect('/verify');
    }
}
