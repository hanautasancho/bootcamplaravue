<?php

namespace App\Listeners;

use App\Events\UserRegenerated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\UserRegeneratedMail;
use Illuminate\Support\Facades\Mail;

class SendEmailNotificationUserRegenerated implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegenerated  $event
     * @return void
     */
    public function handle(UserRegenerated $event)
    {
        Mail::to($event->user)->send(new UserRegeneratedMail($event->user));
    }
}
