<?php

namespace App\Mail;

use App\OtpCode;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserRegeneratedMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $otp;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->otp = OtpCode::where('user_id', $user->id)->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
                    ->view('send_email_user_regenerated')
                    ->with([
                        'codeOtp' => $this->otp->otp
                    ]);
    }
}
