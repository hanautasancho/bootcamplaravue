<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::namespace('Auth')->prefix('auth')->group(function() {
  Route::post('register', 'RegisterController');
  Route::post('login', 'LoginController');

  // verification otp code
  Route::post('verification', 'VerificationController');

  // regenerate otp code
  Route::post('regenerate-otp', 'RegenerateOtpController');
});

Route::namespace('Profile')->prefix('profile')->middleware('auth:api')->group(function() {
  Route::get('get-profile', 'ProfileController@index');
  Route::post('update-profile', 'ProfileController@update');
});

Route::group([
  'middleware' => 'api',
  'prefix' => 'campaign',
], function() {
  Route::get('random/{count}', 'CampaignController@random');
  Route::post('store', 'CampaignController@store');
  Route::get('/', 'CampaignController@index');
  Route::get('/{id}', 'CampaignController@detail');
});

Route::group([
  'middleware' => 'api',
  'prefix' => 'blog',
], function() {
  Route::get('random/{count}', 'BlogController@random');
  Route::post('store', 'BlogController@store');
  Route::get('/', 'BlogController@index');

});


