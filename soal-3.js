var data1 = {
  nama: "strawberry",
  warna: "merah",
  biji: "tidak",
  harga: 9000,
};

var data2 = {
  nama: "jeruk",
  warna: "oranye",
  biji: "ada",
  harga: 8000,
};

var data3 = {
  nama: "Semangka",
  warna: "Hijau & Merah",
  biji: "ada",
  harga: 10000,
};

var data4 = {
  nama: "Pisang",
  warna: "Kuning",
  biji: "tidak",
  harga: 5000,
};

function Buah(data) {
  return data;
}

var data = new Buah([data1, data2, data3, data4]);

console.log(data[1]);
